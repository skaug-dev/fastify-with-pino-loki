# Fastify With Pino-Loki
Project examples written for the article [Observability, commercial and open source, the state in 10.2022](https://skaug.dev/observability-commercial-and-open-source-in-10-2022/).

## Prerequisites
- NPM with Node.js v14 or v16

## Setting it up
1. `npm i`
1. Depending on what project you're testing, make sure you change the Loki parameters.

## What the project contains
- A project file with a Fastify server listening to `/` with GET method. Returns object `{hello: 'world'}` and logs to Loki upon requests.
- A project file without any frameworks, logs a few messages to Loki.