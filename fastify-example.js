'use strict'
import Fastify from 'fastify';
import pino from 'pino';

// Tested with pino-loki v2.03
const pinoLokiTransport = pino.transport({
  target: "pino-loki",
  options: {
    host: 'http://localhost:3100', // Change if Loki hostname is different
    labels: {application:"test-application-with-fastify"}
  },
});
// Tested with pino v8.6.1
const pinoPretty = pino.transport({
    target: "pino-pretty",
    options: {
      translateTime: 'HH:MM:ss Z',
      ignore: 'pid,hostname'
    }
});

// Combine the streams
// NOTE: By setting the "level", you can choose what level each individual transport will recieve a log
const streams = [
  {level: 'debug', stream: pinoLokiTransport},
  {level: 'debug', stream: pinoPretty}
];

const fastify = Fastify({
  logger: {
    stream: pino.multistream(streams),
    level: 'trace' // Set global log level across all transports
  }
});

fastify.get('/', function (request, reply) {

  // Example of how the data sent to Loki will look:
  // {"level":30,"time":1969065718477,"pid":26892,"reqId":"req-1","someTag":"Some info about the current request","msg":"This is a test"}
  request.log.info({someTag:'Some extra info about the current request' }, "This is a test");
  
  // Let's try to send some error data to Loki
  try {
    throw new Error("Some test error object");
  } catch (error){

    // Example of how data sent to Loki will look:
    // {"level":50,"time":1969065718477,"pid":26892,"reqId":"req-1","err": {"type": "Error", "message": "Some test error object", stack: <strack trace...>},"msg":"This is a test error test!"}
    request.log.error(error, "This is a test error test!");
  
  }

  // Let's return hello world to the user
  reply.send({ hello: 'world' });
});
// If you want this example to be accessible outside of localhost, write the IP/hostname address or `::` 
fastify.listen({ host: "localhost", port: 3030 }, (err, address) => {
  if (err) {
    console.log(err)
    process.exit(1)
  }
})